# golang

golang cli, api and other stuffs

#### benchmark
ab-style benchmark utility. you may use it to check stockengine performance. contact us (demo@stockengine.tech) to obtain testuser & password or try to bruteforce it (on demo instance are demousers and weak passwords)

rep contain public auth client library (pkg/auth) and data models (pkg/models).

```
$./benchmark -user testuser -pass testpass -mode ws -c 1 -n 100000
    0s, processed         56 of 100000, instant      56 rps
    1s, processed       9991 of 100000, instant    9935 rps
    2s, processed      24861 of 100000, instant   14870 rps, avg      12032 rps
    3s, processed      42192 of 100000, instant   17331 rps, avg      13761 rps
    4s, processed      57427 of 100000, instant   15235 rps, avg      14123 rps
    5s, processed      71737 of 100000, instant   14310 rps, avg      14160 rps, remains about       1s
    6s, processed      83209 of 100000, instant   11472 rps, avg      13717 rps, remains about       1s
    7s, processed      92752 of 100000, instant    9543 rps, avg      13127 rps, remains about       0s
Counter data (total 100000):
   command                        result count
 WebSocket                      DOMDelta 99992
       new                        Closed 24989
       new                        ToBook 24992
       new                 ClosedPartial    19
    delete                        Cancel   136
    delete IgnoredIdentificationNotFound 49864
Total req & resp = 100000, elapsed 8s, avg rps =  12932
Percentage of the requests served within a certain time (ms)
   1 192.53
  10 251.95
  25 343.83
  50 496.91
  60 526.40
  75 570.74
  80 584.49
  90 627.01
  95 666.05
  98 705.34
  99 735.57
 100 789.65
```