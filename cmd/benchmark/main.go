package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/stockengine-public/golang/internal/benchmark"
)

func main() {
	user := flag.String("user", "", "user name to login")
	pass := flag.String("pass", "", "password to login")
	restendpoint := flag.String("rest", "https://api.stockengine.tech", "rest api endpoint")
	wsendpoint := flag.String("ws", "wss://api.stockengine.tech", "ws api endpoint")
	mode := flag.String("mode", "ws", "connect mode: ws | rest")
	c := flag.Int("c", 1, "concurrency level")
	n := flag.Int("n", 100, "requests per thread")
	flag.Parse()
	if user == nil || pass == nil {
		flag.PrintDefaults()
		return
	}

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt)

	quit := make(chan struct{})
	var needQuit bool

	switch *mode {
	case "rest":
		go func() {
			benchmark.RestTesting(*user, *pass, *restendpoint, *c, *n, &needQuit)
			close(quit)
		}()
	case "ws":
		go func() {
			benchmark.WSTesting(*user, *pass, *restendpoint, *wsendpoint, *c, *n, &needQuit)
			close(quit)
		}()
	default:
		fmt.Println("unknown mode", *mode)
		close(quit)
	}
	select {
	case <-quit:
	case <-sig:
		fmt.Println("terminating")
		needQuit = true
		<-quit
	}
}
