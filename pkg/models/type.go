package models

import (
	"github.com/gogo/protobuf/proto"
	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

type Type int32

const (
	Type_undefinedType Type = 0
	Type_Limit         Type = 1
)

var Type_name = map[int32]string{
	0: "undefinedType",
	1: "Limit",
}

var Type_value = map[string]int32{
	"undefinedType": 0,
	"Limit":         1,
}

func (x Type) Enum() *Type {
	p := new(Type)
	*p = x
	return p
}

func (x Type) String() string {
	return proto.EnumName(Type_name, int32(x))
}

func (x *Type) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(Type_value, data, "Type")
	if err != nil {
		return err
	}
	*x = Type(value)
	return nil
}

func (x Type) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(x.String())
}

func (x *Type) UnmarshalEasyJSON(in *jlexer.Lexer) {
	in.AddError(x.UnmarshalJSON(in.Raw()))
}
