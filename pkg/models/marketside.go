package models

import (
	"github.com/gogo/protobuf/proto"
	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

type MarketSide int32

const (
	MarketSide_undefinedSide MarketSide = 0
	MarketSide_Taker         MarketSide = 1
	MarketSide_Maker         MarketSide = 2
)

var MarketSide_name = map[int32]string{
	0: "undefinedSide",
	1: "Taker",
	2: "Maker",
}

var MarketSide_value = map[string]int32{
	"undefinedSide": 0,
	"Taker":         1,
	"Maker":         2,
}

func (x MarketSide) Enum() *MarketSide {
	p := new(MarketSide)
	*p = x
	return p
}

func (x MarketSide) String() string {
	return proto.EnumName(MarketSide_name, int32(x))
}

func (x *MarketSide) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(MarketSide_value, data, "MarketSide")
	if err != nil {
		return err
	}
	*x = MarketSide(value)
	return nil
}

func (x MarketSide) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(x.String())
}

func (x *MarketSide) UnmarshalEasyJSON(in *jlexer.Lexer) {
	in.AddError(x.UnmarshalJSON(in.Raw()))
}
