package models

//easyjson:json
type CommitedTransaction struct {
	CommitID    uint64      `protobuf:"varint,1,req,name=CommitID" json:"CommitID"`
	Transaction Transaction `protobuf:"bytes,2,req,name=Transaction" json:"Transaction"`
}
type Transaction struct {
	Command         Command    `protobuf:"bytes,1,req,name=Command" json:"Command"`
	OrderFinalState OrderState `protobuf:"bytes,2,req,name=OrderFinalState" json:"OrderFinalState"`
	TxList          []TxDeal   `protobuf:"bytes,3,rep,name=TxList" json:"TxList"`
}
type Command struct {
	ReqID       ReqID       `protobuf:"bytes,1,req,name=ReqID" json:"ReqID"`
	SubCommands SubCommands `protobuf:"bytes,2,req,name=SubCommands" json:"SubCommands"`
}
type OrderState struct {
	AmountFinal Amount      `protobuf:"bytes,1,req,name=AmountFinal" json:"AmountFinal"`
	OrderStatus OrderStatus `protobuf:"varint,2,req,name=OrderStatus,enum=proto.OrderStatus" json:"OrderStatus"`
}
type TxDeal struct {
	Maker OrderChange `protobuf:"bytes,1,req,name=Maker" json:"Maker"`
	Taker OrderChange `protobuf:"bytes,2,req,name=Taker" json:"Taker"`
}
type OrderChange struct {
	MarketSide   MarketSide  `protobuf:"varint,1,req,name=MarketSide,enum=proto.MarketSide" json:"MarketSide"`
	AmountChange Amount      `protobuf:"bytes,2,req,name=AmountChange" json:"AmountChange"`
	OrderStatus  OrderStatus `protobuf:"varint,3,req,name=OrderStatus,enum=proto.OrderStatus" json:"OrderStatus"`
	DealPrice    Price       `protobuf:"bytes,4,req,name=DealPrice" json:"DealPrice"`
	Order        NewOrder    `protobuf:"bytes,5,req,name=Order" json:"Order"`
}
type NewOrder struct {
	Type      Type      `protobuf:"varint,1,req,name=Type,enum=proto.Type" json:"Type"`
	Operation Operation `protobuf:"varint,2,req,name=Operation,enum=proto.Operation" json:"Operation"`
	Amount    Amount    `protobuf:"bytes,3,req,name=Amount" json:"Amount"`
	Price     Price     `protobuf:"bytes,4,req,name=Price" json:"Price"`
	OwnerID   uint64    `protobuf:"varint,5,req,name=OwnerID" json:"OwnerID"`
	ExtID     uint64    `protobuf:"varint,6,req,name=ExtID" json:"ExtID"`
}
type ReqID struct {
	IPLow      uint32 `protobuf:"varint,1,req,name=IPLow" json:"IPLow"`
	SrcDstPort uint32 `protobuf:"varint,2,req,name=SrcDstPort" json:"SrcDstPort"`
	ConnectTs  uint32 `protobuf:"varint,3,req,name=ConnectTs" json:"ConnectTs"`
	AcceptCnt  uint64 `protobuf:"varint,4,req,name=AcceptCnt" json:"AcceptCnt"`
}

//easyjson:json
type LoginPass struct {
	Login    string `protobuf:"bytes,1,req,name=Login" json:"Login"`
	Password string `protobuf:"bytes,2,req,name=Password" json:"Password"`
}

//easyjson:json
type AuthToken struct {
	Login      string   `protobuf:"bytes,1,req,name=Login" json:"Login"`
	Expiration uint64   `protobuf:"varint,2,req,name=Expiration" json:"Expiration"`
	IssuedAt   uint64   `protobuf:"varint,3,req,name=IssuedAt" json:"IssuedAt"`
	Owner      uint64   `protobuf:"varint,4,req,name=Owner" json:"Owner"`
	Role       AuthRole `protobuf:"varint,5,req,name=Role,enum=proto.AuthRole" json:"Role"`
	Generation uint64   `protobuf:"varint,6,req,name=Generation" json:"Generation"`
}

//easyjson:json
type AccessToken struct {
	Owner      uint64   `protobuf:"varint,1,req,name=Owner" json:"Owner"`
	Expiration uint64   `protobuf:"varint,2,req,name=Expiration" json:"Expiration"`
	IssuedAt   uint64   `protobuf:"varint,3,req,name=IssuedAt" json:"IssuedAt"`
	Role       AuthRole `protobuf:"varint,4,req,name=Role,enum=proto.AuthRole" json:"Role"`
}

//easyjson:json
type RefreshTokenCoded struct {
	Access string `protobuf:"bytes,1,req,name=Access" json:"Access"`
	Auth   string `protobuf:"bytes,2,req,name=Auth" json:"Auth"`
}

//easyjson:json
type AuthTokenCoded struct {
	Auth string `protobuf:"bytes,1,req,name=Auth" json:"Auth"`
}

//easyjson:json
type AccessTokenCoded struct {
	Access string `protobuf:"bytes,1,req,name=Access" json:"Access"`
}

//easyjson:json
type DOMBook struct {
	CommitID uint64    `protobuf:"varint,1,req,name=CommitID" json:"CommitID"`
	Bid      []DOMLine `protobuf:"bytes,2,rep,name=Bid" json:"Bid"`
	Ask      []DOMLine `protobuf:"bytes,3,rep,name=Ask" json:"Ask"`
}

//easyjson:json
type DOMDelta struct {
	CommitID uint64         `protobuf:"varint,1,req,name=CommitID" json:"CommitID"`
	Bid      []DOMDeltaElem `protobuf:"bytes,2,rep,name=Bid" json:"Bid"`
	Ask      []DOMDeltaElem `protobuf:"bytes,3,rep,name=Ask" json:"Ask"`
}
type DOMDeltaElem struct {
	DOMOrdersLine DOMLine      `protobuf:"bytes,1,req,name=DOMOrdersLine" json:"DOMOrdersLine"`
	Type          DOMDeltaType `protobuf:"varint,2,req,name=Type,enum=proto.DOMDeltaType" json:"Type"`
}

//easyjson:json
type DOMLine struct {
	Count  uint64 `protobuf:"varint,1,req,name=Count" json:"Count"`
	Price  Price  `protobuf:"bytes,2,req,name=Price" json:"Price"`
	Amount Amount `protobuf:"bytes,3,req,name=Amount" json:"Amount"`
}

//easyjson:json
type SubCommands struct {
	New    *NewOrder    `protobuf:"bytes,1,opt,name=New" json:"New,omitempty"`
	Delete *DeleteOrder `protobuf:"bytes,2,opt,name=Delete" json:"Delete,omitempty"`
	Edit   *EditOrder   `protobuf:"bytes,3,opt,name=Edit" json:"Edit,omitempty"`
}

type DeleteOrder struct {
	OwnerID uint64 `protobuf:"varint,1,req,name=OwnerID" json:"OwnerID"`
	ExtID   uint64 `protobuf:"varint,2,req,name=ExtID" json:"ExtID"`
}
type EditOrder struct {
	OwnerID   uint64 `protobuf:"varint,1,req,name=OwnerID" json:"OwnerID"`
	ExtID     uint64 `protobuf:"varint,2,req,name=ExtID" json:"ExtID"`
	NewAmount Amount `protobuf:"bytes,3,req,name=NewAmount" json:"NewAmount"`
	NewPrice  Price  `protobuf:"bytes,4,req,name=NewPrice" json:"NewPrice"`
}
