package models

import (
	"github.com/gogo/protobuf/proto"
	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

type OrderStatus int32

const (
	OrderStatus_undefinedStatus               OrderStatus = 0
	OrderStatus_Closed                        OrderStatus = 1
	OrderStatus_Cancel                        OrderStatus = 2
	OrderStatus_Changed                       OrderStatus = 3
	OrderStatus_ToBook                        OrderStatus = 4
	OrderStatus_ClosedPartial                 OrderStatus = 5
	OrderStatus_IgnoredDublicateId            OrderStatus = 6
	OrderStatus_IgnoredInvalidPrice           OrderStatus = 7
	OrderStatus_IgnoredInvalidAmount          OrderStatus = 8
	OrderStatus_IgnoredInvalidIdentification  OrderStatus = 9
	OrderStatus_IgnoredIdentificationNotFound OrderStatus = 10
	OrderStatus_ChangedAmount                 OrderStatus = 11
	OrderStatus_ChangedPrice                  OrderStatus = 12
	OrderStatus_ChangedAmountPrice            OrderStatus = 13
	OrderStatus_IgnoredInvalidAmountPrice     OrderStatus = 14
)

var OrderStatus_name = map[int32]string{
	0:  "undefinedStatus",
	1:  "Closed",
	2:  "Cancel",
	3:  "Changed",
	4:  "ToBook",
	5:  "ClosedPartial",
	6:  "IgnoredDublicateId",
	7:  "IgnoredInvalidPrice",
	8:  "IgnoredInvalidAmount",
	9:  "IgnoredInvalidIdentification",
	10: "IgnoredIdentificationNotFound",
	11: "ChangedAmount",
	12: "ChangedPrice",
	13: "ChangedAmountPrice",
	14: "IgnoredInvalidAmountPrice",
}

var OrderStatus_value = map[string]int32{
	"undefinedStatus":               0,
	"Closed":                        1,
	"Cancel":                        2,
	"Changed":                       3,
	"ToBook":                        4,
	"ClosedPartial":                 5,
	"IgnoredDublicateId":            6,
	"IgnoredInvalidPrice":           7,
	"IgnoredInvalidAmount":          8,
	"IgnoredInvalidIdentification":  9,
	"IgnoredIdentificationNotFound": 10,
	"ChangedAmount":                 11,
	"ChangedPrice":                  12,
	"ChangedAmountPrice":            13,
	"IgnoredInvalidAmountPrice":     14,
}

func (x OrderStatus) Enum() *OrderStatus {
	p := new(OrderStatus)
	*p = x
	return p
}

func (x OrderStatus) String() string {
	return proto.EnumName(OrderStatus_name, int32(x))
}

func (x *OrderStatus) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(OrderStatus_value, data, "OrderStatus")
	if err != nil {
		return err
	}
	*x = OrderStatus(value)
	return nil
}

func (x OrderStatus) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(x.String())
}

func (x *OrderStatus) UnmarshalEasyJSON(in *jlexer.Lexer) {
	in.AddError(x.UnmarshalJSON(in.Raw()))
}
