package models

import (
	"github.com/gogo/protobuf/proto"
	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

type Operation int32

const (
	Operation_undefinedOperation Operation = 0
	Operation_Buy                Operation = 1
	Operation_Sell               Operation = 2
)

var Operation_name = map[int32]string{
	0: "undefinedOperation",
	1: "Buy",
	2: "Sell",
}

var Operation_value = map[string]int32{
	"undefinedOperation": 0,
	"Buy":                1,
	"Sell":               2,
}

func (x Operation) Enum() *Operation {
	p := new(Operation)
	*p = x
	return p
}

func (x Operation) String() string {
	return proto.EnumName(Operation_name, int32(x))
}

func (x *Operation) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(Operation_value, data, "Operation")
	if err != nil {
		return err
	}
	*x = Operation(value)
	return nil
}

func (x Operation) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(x.String())
}

func (x *Operation) UnmarshalEasyJSON(in *jlexer.Lexer) {
	in.AddError(x.UnmarshalJSON(in.Raw()))
}
