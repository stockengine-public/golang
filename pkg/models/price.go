package models

import (
	"strconv"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

type Price struct {
	Price float64 `json:"Price"`
}

func (a Price) MarshalEasyJSON(w *jwriter.Writer) {
	str := strconv.FormatFloat(a.Price, 'f', 10, 64)
	w.RawString(str)
}

func (a *Price) UnmarshalEasyJSON(in *jlexer.Lexer) {
	a.Price = in.Float64()
}
