package models

import (
	"github.com/gogo/protobuf/proto"
	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

type AuthRole int32

const (
	AuthRole_unknownRole AuthRole = 0
	AuthRole_DemoRole    AuthRole = 1
)

var AuthRole_name = map[int32]string{
	0: "unknownRole",
	1: "DemoRole",
}

var AuthRole_value = map[string]int32{
	"unknownRole": 0,
	"DemoRole":    1,
}

func (x AuthRole) Enum() *AuthRole {
	p := new(AuthRole)
	*p = x
	return p
}

func (x AuthRole) String() string {
	return proto.EnumName(AuthRole_name, int32(x))
}

func (x *AuthRole) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(AuthRole_value, data, "AuthRole")
	if err != nil {
		return err
	}
	*x = AuthRole(value)
	return nil
}

func (x AuthRole) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(x.String())
}

func (x *AuthRole) UnmarshalEasyJSON(in *jlexer.Lexer) {
	in.AddError(x.UnmarshalJSON(in.Raw()))
}
