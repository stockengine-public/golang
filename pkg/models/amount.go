package models

import (
	"math/big"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

type Amount struct {
	big.Float //stockengine internal presentation -> fixed point with 20 decimal
}

func (a Amount) MarshalEasyJSON(w *jwriter.Writer) {
	txt := a.Text('f', 20)
	w.Raw([]byte(txt), nil)
}

func (a *Amount) UnmarshalEasyJSON(in *jlexer.Lexer) {
	in.AddError(a.UnmarshalText(in.Raw()))
}
