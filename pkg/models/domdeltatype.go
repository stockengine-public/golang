package models

import (
	"github.com/gogo/protobuf/proto"
	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

type DOMDeltaType int32

const (
	DOMDeltaType_Add DOMDeltaType = 0
	DOMDeltaType_Sub DOMDeltaType = 1
)

var DOMDeltaType_name = map[int32]string{
	0: "Add",
	1: "Sub",
}

var DOMDeltaType_value = map[string]int32{
	"Add": 0,
	"Sub": 1,
}

func (x DOMDeltaType) Enum() *DOMDeltaType {
	p := new(DOMDeltaType)
	*p = x
	return p
}

func (x DOMDeltaType) String() string {
	return proto.EnumName(DOMDeltaType_name, int32(x))
}

func (x *DOMDeltaType) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(DOMDeltaType_value, data, "DOMDeltaType")
	if err != nil {
		return err
	}
	*x = DOMDeltaType(value)
	return nil
}

func (x DOMDeltaType) MarshalEasyJSON(w *jwriter.Writer) {
	w.String(x.String())
}

func (x *DOMDeltaType) UnmarshalEasyJSON(in *jlexer.Lexer) {
	in.AddError(x.UnmarshalJSON(in.Raw()))
}
