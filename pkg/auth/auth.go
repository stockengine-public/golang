package auth

import (
	"encoding/base64"
	"strings"
	"time"

	"github.com/mailru/easyjson"

	"github.com/valyala/fasthttp"
	"gitlab.com/stockengine-public/golang/pkg/models"
)

type Auth struct {
	endpoint string
	httpConn *fasthttp.Client

	loginPass models.LoginPass
	codedAuth models.AuthTokenCoded
	refresh   models.AuthToken

	coded  models.RefreshTokenCoded
	access models.AccessToken
}

var defEndpoint = "https://api.stockengine.tech"
var defLogin = "/auth/login"
var defRefresh = "/auth/refresh"

const timeout = 1 * time.Second
const AccessHeader = "X-Access-Token"

func New(login, password, endpoint string, client *fasthttp.Client) *Auth {
	if endpoint == "" {
		endpoint = defEndpoint
	}
	return &Auth{
		loginPass: models.LoginPass{
			Login:    login,
			Password: password,
		},
		endpoint: endpoint,
		httpConn: client,
	}
}

func (t *Auth) AccessToken() (header, token string, err error) {
	if err = t.checkRefresh(); err != nil {
		return
	}
	if err = t.checkAccess(); err != nil {
		return
	}
	header = AccessHeader
	token = t.coded.Access
	return
}

func (t *Auth) checkRefresh() error {
	if t.refresh.Expiration > uint64(time.Now().Unix()) {
		return nil
	}
	return t.updateRefresh()
}
func (t *Auth) checkAccess() error {
	if t.access.Expiration+1 > uint64(time.Now().Unix()) {
		return nil
	}
	return t.updateAccess() //it will be valid one second at least
}
func (t *Auth) updateRefresh() (err error) {
	if err = helperRequest(&t.loginPass, &t.codedAuth, t.endpoint+defLogin, t.httpConn, ""); err != nil {
		return
	}
	return helperDecode(t.codedAuth.Auth, &t.refresh)
}
func (t *Auth) updateAccess() (err error) {
	if err = helperRequest(&t.codedAuth, &t.coded, t.endpoint+defRefresh, t.httpConn, t.codedAuth.Auth); err != nil {
		return
	}
	t.codedAuth.Auth = t.coded.Auth
	if err = helperDecode(t.coded.Auth, &t.refresh); err != nil {
		return
	}
	if err = helperDecode(t.coded.Access, &t.access); err != nil {
		return
	}
	return
}

func helperRequest(request easyjson.Marshaler, response easyjson.Unmarshaler, url string, client *fasthttp.Client, authToken string) (err error) {
	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseRequest(req)
	defer fasthttp.ReleaseResponse(resp)

	if request != nil {
		if _, err = easyjson.MarshalToWriter(request, req.BodyWriter()); err != nil {
			return
		}
	}
	req.SetRequestURI(url)
	req.Header.SetMethod(fasthttp.MethodPost)
	if authToken != "" {
		req.Header.Set(fasthttp.HeaderAuthorization, "Bearer "+authToken)
	}
	err = client.DoTimeout(req, resp, timeout)
	switch {
	case err != nil:
		return err
	case resp.StatusCode() != fasthttp.StatusOK || resp.Body() == nil:
		return ErrBadServerResp
	default:
		//do nothing
	}
	return easyjson.Unmarshal(resp.Body(), response)
}

func helperDecode(field string, unmarshaler easyjson.Unmarshaler) error {
	//decode refresh & access via footer
	lastDot := strings.LastIndexByte(field, '.')
	if lastDot == -1 {
		return ErrInvalidFooter
	}
	base64Str := field[lastDot+1:]
	decodedBase64, err := base64.RawStdEncoding.DecodeString(base64Str)
	if err != nil {
		return err
	}
	return easyjson.Unmarshal(decodedBase64, unmarshaler)
}

func (t *Auth) DecodeAuthToken() models.AuthToken {
	return t.refresh
}
func (t *Auth) DecodeAccessToken() models.AccessToken {
	return t.access
}
