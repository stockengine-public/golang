// +build integrationTest

package auth

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/valyala/fasthttp"
)

const testUser = ""
const testPasswd = ""

var defaultClient = &fasthttp.Client{
	Name:         "stockengine benchmark client",
	ReadTimeout:  timeout,
	WriteTimeout: timeout,
}

func TestAuth_AccessToken(t *testing.T) {
	auth := New(testUser, testPasswd, "", defaultClient)
	h, tk, e := auth.AccessToken()
	require.NoError(t, e)
	assert.NotEmpty(t, h)
	assert.NotEmpty(t, tk)
}

func BenchmarkAuth_AccessToken(b *testing.B) {
	auth := New(testUser, testPasswd, "", defaultClient)
	h, tk, e := auth.AccessToken()
	require.NoError(b, e)
	assert.NotEmpty(b, h)
	assert.NotEmpty(b, tk)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		h, tk, e := auth.AccessToken()
		if e != nil || h == "" || tk == "" {
			b.FailNow()
		}
	}
}
