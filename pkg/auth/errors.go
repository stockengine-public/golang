package auth

import "errors"

var ErrInvalidFooter = errors.New("invalid footer")
var ErrBadServerResp = errors.New("invalid server resp")
