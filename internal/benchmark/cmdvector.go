package benchmark

import (
	"gitlab.com/stockengine-public/golang/pkg/models"
)

func (t *Benchmark) getCmdVector() (ret []*models.SubCommands) {
	randAmount := t.RandAmount()
	randPrice := t.RandPrice()
	nextExtId1 := t.NextExtId()

	ret = append(ret, &models.SubCommands{
		New: &models.NewOrder{
			Type:      models.Type_Limit,
			Operation: models.Operation_Buy,
			Amount:    randAmount,
			Price:     randPrice,
			ExtID:     nextExtId1,
		},
	})

	nextExtId2 := t.NextExtId()

	//the same apount and price, but reverse operation
	ret = append(ret, &models.SubCommands{
		New: &models.NewOrder{
			Type:      models.Type_Limit,
			Operation: models.Operation_Sell,
			Amount:    randAmount,
			Price:     randPrice,
			ExtID:     nextExtId2,
		},
	})

	ret = append(ret, &models.SubCommands{
		Delete: &models.DeleteOrder{
			ExtID: nextExtId1,
		},
	})

	ret = append(ret, &models.SubCommands{
		Delete: &models.DeleteOrder{
			ExtID: nextExtId2,
		},
	})
	return
}
