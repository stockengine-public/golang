package benchmark

import (
	"gitlab.com/stockengine-public/golang/pkg/models"
)

//Create buy order & create sell order, that will close it
//and after all delete all orders
func (t *Benchmark) RestCreateCloseDeleteOrder() (err error) {

	vec := t.getCmdVector()
	for _, sc := range vec {
		result := &models.CommitedTransaction{}
		err, elapsed := t.helperSendRequest(sc, result, t.rest+restOrder)
		res := resultToString(result)
		if err != nil {
			res = err.Error()
		}
		t.ObserveLatency(elapsed, commandToString(sc), res)
	}

	return
}

func (t *Benchmark) RestDeleteAll() {
	sc := &models.SubCommands{
		Delete: &models.DeleteOrder{},
	}
	result := &models.CommitedTransaction{}
	if err, _ := t.helperSendRequest(sc, result, t.rest+restOrder); err != nil {
		return
	}
	//b, _ := easyjson.Marshal(result)
	//fmt.Printf("delete 2 -> %s\n", string(b))
}
