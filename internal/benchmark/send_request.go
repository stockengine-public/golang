package benchmark

import (
	"fmt"
	"time"

	"github.com/mailru/easyjson"
	"github.com/valyala/fasthttp"
)

func (t *Benchmark) helperSendRequest(request easyjson.Marshaler, response easyjson.Unmarshaler, url string) (err error, elapsed time.Duration) {
	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseRequest(req)
	defer fasthttp.ReleaseResponse(resp)

	if request != nil {
		if _, err = easyjson.MarshalToWriter(request, req.BodyWriter()); err != nil {
			return
		}
	}
	req.SetRequestURI(url)
	req.Header.SetMethod(fasthttp.MethodPost)

	h, tk, err := t.AccessToken()
	if err != nil {
		return
	}

	req.Header.Set(h, tk)
	start := time.Now()
	err = t.httpConn.DoTimeout(req, resp, timeout)
	elapsed = time.Since(start)
	switch {
	case err != nil:
		switch err {
		case fasthttp.ErrConnectionClosed:
			err = fmt.Errorf("code[%d]", 499)
		default:
		}
		return
	case resp.StatusCode() != fasthttp.StatusOK:
		switch resp.StatusCode() {
		case fasthttp.StatusBadRequest:
			err = fmt.Errorf("code[%d] body[%s]", resp.StatusCode(), string(resp.Body()))
		case fasthttp.StatusUnauthorized, fasthttp.StatusBadGateway:
			err = fmt.Errorf("code[%d]", resp.StatusCode())
		default:
			err = fmt.Errorf("code[%d] body[%s]", resp.StatusCode(), string(resp.Body()))
		}
		return
	default:
		//do nothing
	}
	err = easyjson.Unmarshal(resp.Body(), response)
	return
}
