package benchmark

func (t *Benchmark) AccessToken() (string, string, error) {
	t.mx.Lock()
	defer t.mx.Unlock()

	return t.auth.AccessToken()
}
