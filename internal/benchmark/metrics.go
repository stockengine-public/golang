package benchmark

import (
	"fmt"
	"os"
	"text/tabwriter"
	"time"

	"gitlab.com/stockengine-public/golang/pkg/models"
)

type commandStats struct {
	cnt map[string]uint64
}

func (t *Benchmark) ObserveLatency(duration time.Duration, command, result string) {
	t.metricsLock.Lock()
	defer t.metricsLock.Unlock()
	t.quantiles.Add(duration.Seconds(), 1)
	elem := t.counters[command]
	if elem.cnt == nil {
		elem.cnt = make(map[string]uint64)
	}
	elem.cnt[result]++
	t.counters[command] = elem
	t.total++

	if t.startTime.Equal(time.Time{}) {
		t.startTime = time.Now()
	}
}
func (t *Benchmark) ObserveEvent(event string) {
	const command = "WebSocket"
	t.metricsLock.Lock()
	defer t.metricsLock.Unlock()
	elem := t.counters[command]
	if elem.cnt == nil {
		elem.cnt = make(map[string]uint64)
	}
	elem.cnt[event]++
	t.counters[command] = elem
}
func (t *Benchmark) Print() {
	t.metricsLock.Lock()
	defer t.metricsLock.Unlock()
	elapsed := time.Since(t.startTime)
	//counters
	fmt.Printf("Counter data (total %d):\n", t.total)
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.AlignRight)
	_, _ = fmt.Fprintf(w, "%s\t%s\t%s\t\n", "command", "result", "count")
	for k, v := range t.counters {
		for k2, v2 := range v.cnt {
			_, _ = fmt.Fprintf(w, "%s\t%s\t%d\t\n", k, k2, v2)
		}
	}
	_ = w.Flush()

	rps := float64(t.total) / elapsed.Seconds()
	fmt.Printf("Total req & resp = %d, elapsed %s, avg rps = %6.0f\n", t.total, elapsed.Round(time.Second), rps)

	//latencies
	w = tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.AlignRight)
	quantiles := []float64{0.01, 0.1, 0.25, 0.5, 0.6, 0.75, 0.8, 0.9, 0.95, 0.98, 0.99, 1.0}
	fmt.Println("Percentage of the requests served within a certain time (ms)")
	for _, q := range quantiles {
		val := t.quantiles.Quantile(q)
		_, _ = fmt.Fprintf(w, "%2.0f\t%4.2f\t\n", q*100, val*1000)
	}
	_ = w.Flush()
}

func commandToString(sc *models.SubCommands) string {
	switch {
	case sc.New != nil:
		return "new"
	case sc.Delete != nil:
		return "delete"
	case sc.Edit != nil:
		return "edit"
	default:
		return "unknown"
	}
}
func resultToString(result *models.CommitedTransaction) string {
	return result.Transaction.OrderFinalState.OrderStatus.String()
}

func (t *Benchmark) Reporter(max int) {
	go func() {
		ticker := time.NewTicker(time.Second)
		var lastCnt uint64
		for {
			select {
			case <-ticker.C:
				t.metricsLock.Lock()
				cnt := t.total
				elapsed := time.Since(t.startTime)
				t.metricsLock.Unlock()
				instRps := cnt - lastCnt
				lastCnt = cnt
				if cnt == 0 {
					continue
				}
				fmt.Printf("%5ds, processed %10d of %d, instant %7d rps", int(elapsed.Seconds()), cnt, max, instRps)
				if elapsed.Seconds() > 2 {
					fmt.Printf(", avg %10.0f rps", float64(cnt)/elapsed.Seconds())
				}
				if cnt > 100 && elapsed.Seconds() > 5 {
					remain := time.Duration((max*int(elapsed.Seconds())/int(cnt) - int(elapsed.Seconds())) * 1e9)
					fmt.Printf(", remains about %8s", remain.Round(time.Second).String())
				}
				fmt.Println()
			case <-t.quit:
				return
			}
		}
	}()

}
