package benchmark

import (
	"sync"
	"time"

	"github.com/valyala/fasthttp"
	auth2 "gitlab.com/stockengine-public/golang/pkg/auth"
)

func RestTesting(user, pass, endpoint string, c, n int, needQuit *bool) {
	client := &fasthttp.Client{
		Name:                          "test stockengine client",
		NoDefaultUserAgentHeader:      false,
		Dial:                          nil,
		DialDualStack:                 false,
		TLSConfig:                     nil,
		MaxConnsPerHost:               10000,
		MaxIdleConnDuration:           0,
		MaxConnDuration:               time.Second * 10,
		MaxIdemponentCallAttempts:     1,
		ReadBufferSize:                0,
		WriteBufferSize:               0,
		ReadTimeout:                   time.Second,
		WriteTimeout:                  time.Second,
		MaxResponseBodySize:           0,
		DisableHeaderNamesNormalizing: false,
		DisablePathNormalizing:        false,
	}
	auth := auth2.New(user, pass, endpoint, client)
	bench := New(auth, endpoint, "", client)

	wg := sync.WaitGroup{}

	bench.Reporter(c * n)
	n /= len(bench.getCmdVector())

	wg.Add(c)
	for z := 0; z < c; z++ {
		go func() {
			defer wg.Done()
			for i := 0; i < n && !*needQuit; i++ {
				if err := bench.RestCreateCloseDeleteOrder(); err != nil {
					//fmt.Printf("err -> %s\n", err)
					//return
				}
			}
		}()
	}
	wg.Wait()
	close(bench.quit)

	bench.RestDeleteAll()
	bench.Print()
}
