package benchmark

import (
	"math/big"
	"math/rand"
	"sync/atomic"
	"time"

	"gitlab.com/stockengine-public/golang/pkg/models"
)

func (t *Benchmark) InitRand() {
	src := rand.NewSource(time.Now().Unix())
	t.rnd = rand.New(src)
	t.nextId = uint64(time.Now().UnixNano()) // to minimize overlapping risk
}

func (t *Benchmark) RandAmount() models.Amount {
	t.rndLock.Lock()
	bf := big.NewFloat(float64(t.rnd.Int63n(10000000000)+1) / 1000000000.0)
	t.rndLock.Unlock()
	return models.Amount{Float: *bf}
}

func (t *Benchmark) RandPrice() models.Price {
	t.rndLock.Lock()
	f := float64(t.rnd.Int63n(1000000)+1) / 100000
	t.rndLock.Unlock()
	return models.Price{Price: f}
}
func (t *Benchmark) NextExtId() uint64 {
	return atomic.AddUint64(&t.nextId, 1)
}
