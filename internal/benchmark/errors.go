package benchmark

import "errors"

var ErrBadServerResp = errors.New("invalid server resp")
var ErrBadAuth = errors.New("invalid auth data -> 401 server resp")
