// +build integrationTest

package benchmark

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/valyala/fasthttp"
	auth2 "gitlab.com/stockengine-public/golang/pkg/auth"
)

const testUser = ""
const testPasswd = ""

func TestBenchmark_NewOrder(t *testing.T) {
	client := &fasthttp.Client{
		Name:         "test stockengine client",
		ReadTimeout:  timeout,
		WriteTimeout: timeout,
	}
	auth := auth2.New(testUser, testPasswd, "", client)
	bench := New(auth, "", client)

	assert.NoError(t, bench.RestCreateCloseDeleteOrder())
}

func BenchmarkNew(b *testing.B) {
	client := &fasthttp.Client{
		Name:         "test stockengine client",
		ReadTimeout:  timeout,
		WriteTimeout: timeout,
	}
	auth := auth2.New(testUser, testPasswd, "", client)
	bench := New(auth, "", client)

	require.NoError(b, bench.RestCreateCloseDeleteOrder())
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if err := bench.RestCreateCloseDeleteOrder(); err != nil {
			b.Fatal(err)
		}
	}
}
