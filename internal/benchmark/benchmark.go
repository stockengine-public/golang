package benchmark

import (
	"math/rand"
	"sync"
	"time"

	"github.com/influxdata/tdigest"

	"github.com/valyala/fasthttp"
	auth2 "gitlab.com/stockengine-public/golang/pkg/auth"
)

type Benchmark struct {
	auth     *auth2.Auth
	rest     string
	ws       string
	httpConn *fasthttp.Client
	mx       sync.Mutex
	rnd      *rand.Rand
	rndLock  sync.Mutex
	nextId   uint64

	metricsLock sync.Mutex
	quantiles   *tdigest.TDigest
	counters    map[string]commandStats
	startTime   time.Time
	total       uint64
	quit        chan struct{}
}

var defRestEndpoint = "https://api.stockengine.tech"
var defWSEndpoint = "wss://api.stockengine.tech"
var restOrder = "/rest/order"

const timeout = 1 * time.Second

func New(auth *auth2.Auth, rest, ws string, client *fasthttp.Client) *Benchmark {
	if rest == "" {
		rest = defRestEndpoint
	}
	if ws == "" {
		ws = defWSEndpoint
	}
	ret := &Benchmark{
		auth:      auth,
		rest:      rest,
		ws:        ws,
		httpConn:  client,
		quantiles: tdigest.New(),
		counters:  make(map[string]commandStats),
		quit:      make(chan struct{}),
	}
	ret.InitRand()

	return ret
}
