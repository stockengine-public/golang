package benchmark

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"github.com/mailru/easyjson"
	"gitlab.com/stockengine-public/golang/pkg/models"

	"github.com/valyala/fasthttp"

	"github.com/gorilla/websocket"
)

var defWsOrder = "/ws/order"
var defWsDelta = "/ws/dom/delta"
var defWsTop100 = "/ws/dom/top100"

func (t *Benchmark) WSCreateCloseDeleteOrder(count int, needStop *bool) (err error) {
	newProcessingStart := sync.Map{} //make(map[uint64]time.Time)
	editProcessingStart := sync.Map{}
	delProcessingStart := sync.Map{}
	getMap := func(sc *models.SubCommands) *sync.Map {
		switch {
		case sc.New != nil:
			return &newProcessingStart
		case sc.Edit != nil:
			return &editProcessingStart
		case sc.Delete != nil:
			return &delProcessingStart
		default:
			return nil //lets crush
		}
	}
	h, tk, err := t.AccessToken()
	if err != nil {
		return
	}

	dialer := &websocket.Dialer{
		HandshakeTimeout: timeout,
	}
	headers := http.Header{}
	headers.Set(h, tk)
	conn, resp, err := dialer.Dial(t.ws+defWsOrder, headers)
	if resp != nil {
		if resp.StatusCode == fasthttp.StatusUnauthorized {
			return ErrBadAuth
		}
		//fmt.Println("resp -> ", resp.StatusCode, resp.Proto, resp.ContentLength)
	}
	if conn != nil {
		//fmt.Println("conn -> ", conn.RemoteAddr().String(), conn.LocalAddr().String())
		defer func() {
			e := conn.Close()
			if e != nil {
				fmt.Println("close err -> ", e.Error())
			}
		}()
	}

	if err != nil {
		return
	}

	//var b []byte
	if resp != nil && resp.Body != nil {
		_, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return
		}
	}
	//fmt.Println("body -> ", string(b))

	recCount := count * len(t.getCmdVector())

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()

		for i := 0; i < recCount; i++ {
			if *needStop {
				break
			}
			_, b, err := conn.ReadMessage()
			if err != nil {
				fmt.Println("read message err -> ", err.Error())
				break
			}
			//fmt.Printf("message type[%d] body size[%d] data[%s]\n", mt, len(b), string(b))
			commitedTx := models.CommitedTransaction{}
			err = easyjson.Unmarshal(b, &commitedTx)
			if err != nil {
				fmt.Println("unmarshal message err -> ", err.Error())
				break
			}
			sc := &commitedTx.Transaction.Command.SubCommands
			extID := getSubCommandExtID(sc)
			startedIface, ok := getMap(sc).Load(extID)
			if !ok {
				fmt.Printf("got unexpected extID -> %d\n", extID)
			} else {
				started := startedIface.(time.Time)
				elapsed := time.Since(started)
				t.ObserveLatency(elapsed, commandToString(&commitedTx.Transaction.Command.SubCommands), resultToString(&commitedTx))
				getMap(sc).Delete(extID)
			}
		}
	}()
	wg.Add(1)
	go func() {
		defer wg.Done()
	SendLoop:
		for i := 0; i < count; i++ {
			if *needStop {
				break
			}

			vec := t.getCmdVector()
			for _, cmd := range vec {
				b, err := easyjson.Marshal(cmd)
				if err != nil {
					fmt.Println(err)
					break SendLoop
				}
				start := time.Now()
				err = conn.WriteMessage(websocket.TextMessage, b)
				if err != nil {
					fmt.Println(err)
					break SendLoop
				}
				extID := getSubCommandExtID(cmd)
				_, loaded := getMap(cmd).LoadOrStore(extID, start)
				if loaded {
					fmt.Printf("error!!! duplicate extID [%d] on [%s]\n", extID, commandToString(cmd))
				}
			}
		}
	}()
	wg.Wait()

	delAll := &models.SubCommands{
		Delete: &models.DeleteOrder{},
	}
	b, err := easyjson.Marshal(delAll)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = conn.WriteMessage(websocket.TextMessage, b)
	if err != nil {
		fmt.Println("delete all error", err)
	}

	return
}

func (t *Benchmark) WSGetDeltaStream(needStop *bool) (err error) {

	h, tk, err := t.AccessToken()
	if err != nil {
		return
	}

	dialer := &websocket.Dialer{
		HandshakeTimeout: timeout,
	}
	headers := http.Header{}
	headers.Set(h, tk)
	conn, resp, err := dialer.Dial(t.ws+defWsDelta, headers)
	if resp != nil {
		if resp.StatusCode == fasthttp.StatusUnauthorized {
			return ErrBadAuth
		}
		//fmt.Println("resp -> ", resp.StatusCode, resp.Proto, resp.ContentLength)
	}
	if conn != nil {
		//fmt.Println("conn -> ", conn.RemoteAddr().String(), conn.LocalAddr().String())
		defer func() {
			e := conn.Close()
			if e != nil {
				fmt.Println("close err -> ", e.Error())
			}
		}()
	}

	if err != nil {
		return
	}

	//var b []byte
	if resp != nil && resp.Body != nil {
		_, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return
		}
	}
	//fmt.Println("body -> ", string(b))

	for !*needStop {
		_, b, err := conn.ReadMessage()
		if err != nil {
			fmt.Println("read message err -> ", err.Error())
			break
		}
		//fmt.Printf("message type[%d] body size[%d] data[%s]\n", mt, len(b), string(b))
		delta := models.DOMDelta{}
		err = easyjson.Unmarshal(b, &delta)
		if err != nil {
			fmt.Println("unmarshal message err -> ", err.Error())
			break
		}
		t.ObserveEvent("DOMDelta")
	}

	return
}

func getSubCommandExtID(sub *models.SubCommands) uint64 {
	switch {
	case sub.New != nil:
		return sub.New.ExtID
	case sub.Edit != nil:
		return sub.Edit.ExtID
	case sub.Delete != nil:
		return sub.Delete.ExtID
	default:
		return 0
	}
}
