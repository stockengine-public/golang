package benchmark

import (
	"fmt"
	"sync"
	"time"

	"github.com/valyala/fasthttp"
	auth2 "gitlab.com/stockengine-public/golang/pkg/auth"
)

func WSTesting(user, pass, rest, ws string, c, n int, needStop *bool) {
	client := &fasthttp.Client{
		Name:                      "test stockengine client",
		MaxIdemponentCallAttempts: 1,
		ReadTimeout:               time.Second,
		WriteTimeout:              time.Second,
	}
	auth := auth2.New(user, pass, rest, client)
	bench := New(auth, rest, ws, client)
	var wg sync.WaitGroup
	bench.Reporter(c * n)
	n /= len(bench.getCmdVector())
	deltaStreamDone := make(chan struct{})
	go func() {
		defer close(deltaStreamDone)
		err := bench.WSGetDeltaStream(needStop)
		if err != nil {
			fmt.Println(err)
		}
	}()
	for cc := 0; cc < c; cc++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			err := bench.WSCreateCloseDeleteOrder(n, needStop)
			if err != nil {
				fmt.Println(err)
			}
		}()
	}

	wg.Wait()
	close(bench.quit)
	*needStop = true
	<-deltaStreamDone

	bench.Print()
}
