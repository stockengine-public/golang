module gitlab.com/stockengine-public/golang

go 1.13

require (
	github.com/gogo/protobuf v1.3.1
	github.com/gorilla/websocket v1.4.1
	github.com/influxdata/tdigest v0.0.1
	github.com/mailru/easyjson v0.7.0
	github.com/stretchr/testify v1.4.0
	github.com/valyala/fasthttp v1.7.1
)
